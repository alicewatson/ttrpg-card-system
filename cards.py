from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import matplotlib.colors as mcolors


class Card:
    """An index card that can hold a title, type, and up to 10 lines of body text."""
  
    font: str = 'FreeMono.ttf'

    def __init__(self, card_type, card_title, card_body, card_color='white') -> None:
        self.card_type = card_type
        self.card_title = card_title
        self.card_body = card_body
        self.card_color = card_color
        self.card_type_pos = [750, 15]
        self.title_pos = [10, 10]
        
        self.body_pos = [
            [15,  65],  [15, 103],
            [15, 142],  [15, 180],
            [15, 220],  [15, 258],
            [15, 297],  [15, 335],
            [15, 375],  [15, 415],
        ]

        self._create_card()
    
    def _create_card(self):
        card = Image.open('resources/card.jpeg')

        # Check if the card_color exists in xkcd_colors
        overlay_color = mcolors.XKCD_COLORS.get('xkcd:' + self.card_color.lower(), 'white')  # Default to white if not found

        # Convert the RGBA values to integers (0-255)
        overlay_color = tuple(int(value * 255) for value in mcolors.to_rgba(overlay_color))
        overlay_color = overlay_color[:3] + (128,)

        overlay = Image.new('RGBA', card.size, overlay_color)
        
        card = Image.alpha_composite(card.convert('RGBA'), overlay)

        imgtxt = ImageDraw.Draw(card)

        imgtxt.text(self.title_pos, self.card_title, font=ImageFont.truetype(self.font, 40), fill=(0, 0, 0))
        imgtxt.text([self.card_type_pos[0] - (len(self.card_type)*18), self.card_type_pos[1]], self.card_type, font=ImageFont.truetype(self.font, 30), fill=(0, 0, 0))

        for loc, line in zip(self.body_pos[:len(self.card_body)], self.card_body):
            imgtxt.text(loc, line, font=ImageFont.truetype(self.font, 25), fill=(0, 0, 0))

        self.card = card
    
    def save(self, path):
        self.card.save(path)


## Examples ###############################################

# Campaign example ========================================
campaign = {
    'card_type': 'Campaign',
    'card_title': 'No Mo Mojo Jojo',
    'card_body': [
        '',
        "It's like Dexter meets the PowerPuff Girls",
        '',
        'Players:',
        '  Bubbles (Alice)',
        '  Blossom (Bob)',
        '  Buttercup (Carol)',
        '  The Professor (Doug)',
        '',
        'System: FATE'
    ],
    'card_color': 'white'
}

card = Card(**campaign)
card.save(path='cards/campaign_card.png')

# Townsville Setting =======================================
townsville_setting = {
    'card_type': 'Setting',
    'card_title': 'Townsville, USA',
    'card_body': [
        'Charming American City with a Twist',
        'Home to the [Powerpuff Girls]',
        "The Is's:",
        '  Super-powers, talking animals',
        "The Isn'ts:",
        '  Responsible adults, regular emergency services',
    ],
    'card_color': 'white'
}

townsville_setting_card = Card(**townsville_setting)
townsville_setting_card.save('cards/setting_card_townsville.png')

# Powerpuff Girls Faction =================================
powerpuff_girls_faction = {
    'card_type': 'Faction',
    'card_title': 'Powerpuff Girls',
    'card_body': [
        'Superhero Trio of [Townsville]',
        'Dedicated to Protecting the City',
        '',
        'Motivations:',
        '  Protect [Townsville] from [Villains]',
        '  Uphold Justice and Do Good',
        '  Support each other'
    ],
    'card_color': 'red'
}

powerpuff_girls_faction_card = Card(**powerpuff_girls_faction)
powerpuff_girls_faction_card.save('cards/faction_card_powerpuff_girls.png')

# Villains Faction =========================================
villains_faction = {
    'card_type': 'Faction',
    'card_title': 'Rogues Gallery',
    'card_body': [
        'Rogues and Miscreants of [Townsville]',
        'Constantly Hatching Evil Schemes',
        '',
        'Motivations:',
        '  Defeat the [Powerpuff Girls]',
        '  Achieve World Domination',
        '  Cause Mayhem and Havoc'
    ],
    'card_color': 'red'
}

villains_faction_card = Card(**villains_faction)
villains_faction_card.save('cards/faction_card_villains.png')

# Buttercup example =======================================
character = {
    'card_type': 'Character',
    'card_title': 'Buttercup',
    'card_body': [
        'Superpowered Fighter with an Attitude',
        'Impulsive Recklessness',
        'Aspects:',
        '  Tough Tomboy Attitude',
        '  Green Energy Manipulation',
        '  Impulsive Fighter',
        'Motivations:',
        '  Defend her sisters',
        '  Smash evil and protect [Townsville]',
    ],
    'card_color': 'red'
}

card = Card(**character)
card.save(path='cards/character_card_buttercup.png')

# Blossom example =========================================
character = {
    'card_type': 'Character',
    'card_title': 'Blossom',
    'card_body': [
        'Strategic Leader with a Sense of Responsibility',
        'Perfectionist Tendencies',
        'Aspects:',
        '  Tactical Thinker',
        '  Responsible Sister',
        '  Ice Breath and Eye Beams',
        'Motivations:',
        '  Fulfill duty to protect Townsville',
        '  Lead the [PowerPuff Girls]'
    ],
    'card_color': 'yellow'
}

card = Card(**character)
card.save('cards/character_card_blossom.png')

# Bubbles example ========================================
bubbles_character = {
    'card_type': 'Character',
    'card_title': 'Bubbles',
    'card_body': [
        'Sweet and Sensitive Animal Whisperer',
        'Fear of the Dark',
        'Aspects:',
        '  Animal Whisperer',
        '  Kind-hearted Sister',
        '  Sonic Scream and Bubble Manipulation',
        'Motivations:',
        '  Help those in need',
        '  Enjoy the day',
    ],
    'card_color': 'yellow'
}

bubbles_card = Card(**bubbles_character)
bubbles_card.save('cards/character_card_bubbles.png')

# HIM example =============================================
character = {
    'card_type': 'Character',
    'card_title': 'HIM',
    'card_body': [
        'Mysterious and Manipulative Villain',
        'Gender-Fluid Trickster',
        'Aspects:',
        '  Otherworldly Charm',
        '  Shapeshifter',
        '  Sinister Plotter',
        'Motivations:',
        '  Self amusement',
        '  Spreading the seeds of chaos',
    ],
    'card_color': 'red'
}

card = Card(**character)
card.save(path='cards/character_card_him.png')

# Mojo Jojo example =======================================
mojojojo_character = {
    'card_type': 'Character',
    'card_title': 'Mojo Jojo',
    'card_body': [
        'Brilliant and Evil Simian Mastermind',
        'Obsession with World Domination',
        'Aspects:',
        '  Evil Genius',
        '  Scientific Inventions and Gadgets',
        '  Overconfident Strategist',
        'Motivations:',
        '  Revenge on the [PowerPuff Girls]',
        '  Desire for power and recognition',
    ],
    'card_color': 'yellow'
}

mojojojo_card = Card(**mojojojo_character)
mojojojo_card.save('cards/character_card_mojojojo.png')

# Mojo Jojo's Observatory example ==========================
mojojojo_observatory = {
    'card_type': 'Location',
    'card_title': "Mojo Jojo's Observatory",
    'card_body': [
        'High-Tech Supervillain Lair and Laboratory',
        'Hidden in the Mountains',
        'Aspects:',
        '  Advanced Scientific Equipment',
        '  Remote and Inaccessible Location',
        '  Evil Scheming Hideout',
        '  Home to Devious Experiments',
    ],
    'card_color': 'blue'
}

mojojojo_observatory_card = Card(**mojojojo_observatory)
mojojojo_observatory_card.save('cards/location_card_mojojojo_observatory.png')

# Powerpuff Girls' House example =============================
ppg_house = {
    'card_type': 'Location',
    'card_title': "Powerpuff Girls' House",
    'card_body': [
        'Home of the Crime-Fighting Kindergarten Heroes',
        'Located in Townsville',
        'Aspects:',
        '  Secret Basement Laboratory',
        '  Iconic Phone Hotline',
        '  Safe Haven for the Girls',
        '  Training Ground for Heroes',
        'Parent Location: [Townsville]',
    ],
    'card_color': 'blue'
}

ppg_house_card = Card(**ppg_house)
ppg_house_card.save('cards/location_card_ppg_house.png')

# Secret Basement Laboratory example ========================
secret_lab = {
    'card_type': 'Location',
    'card_title': 'Secret Basement Lab',
    'card_body': [
        "Hidden Lab Beneath the Powerpuff Girls House",
        "[Professor Utonium]'s Scientific Workspace",
        'Aspects:',
        '  Cutting-Edge Scientific Equipment',
        '  Birthplace of the [Powerpuff Girls]',
        '  Highly Secure and Secretive',
        '  Experiments and Inventions Abound',
        "Parent Location: [Powerpuff Girls' House]",
    ],
    'card_color': 'blue'
}

secret_lab_card = Card(**secret_lab)
secret_lab_card.save('cards/location_card_secret_lab.png')

# Chemical X example =======================================
chemical_x = {
    'card_type': 'Object',
    'card_title': 'Chemical X',
    'card_body': [
        'Mysterious Chemical Substance',
        "Source of the Powerpuff Girls' Superpowers",
    ],
    'card_color': 'white'
}

chemical_x_card = Card(**chemical_x)
chemical_x_card.save('cards/object_card_chemical_x.png')

# Fuzzy Lumpkin's Shotgun example =========================
fuzzy_shotgun = {
    'card_type': 'Object',
    'card_title': "Fuzzy Lumpkin's Shotgun",
    'card_body': [
        'A Simple Yet Powerful Shotgun',
        'A Little Too Rustic',
        'Aspects:',
        '  Delivers Devastating Blasts',
        '  Favored Weapon of [Fuzzy Lumpkin]',
    ],
    'card_color': 'white'
}

fuzzy_shotgun_card = Card(**fuzzy_shotgun)
fuzzy_shotgun_card.save('cards/object_card_fuzzy_shotgun.png')

# Plot example ============================================
nomo_mojo = {
    'card_type': 'Plot',
    'card_title': "Oh No, Mojo is No Mo!",
    'card_body': [
        '[Mojo] found dead in his [Observatory]',
        "Shot with [Fuzzy Lumpkin's Shotgun]",
        '[Buttercup] has been framed for the murder',
        '',
        'Clues:',
        "  The gun's trigger has clamp marks on it",
        '  Small black scuffs on the floor',
        '  A person with a strange voice called it in',
        "  [Buttercup]'s hair in [Mojo]'s fist",
    ],
    'card_color': 'purple'
}

nomo_mojo_plot = Card(**nomo_mojo)
nomo_mojo_plot.save('cards/plot_card_nomo_mojo.png')

# Actions example (back of HIM's card) ====================
him_actions = {
    'card_type': 'Actions',
    'card_title': "(back of HIM's card)",
    'card_body': [
        "Plotting to frame [Buttercup]",
        "  Stole [Buttercup]'s hairbrush",
        "  Stole [Fuzzy Lumpkin's Shotgun]",
        "  Murdered [Mojo Jojo]",
        "  Planted [Buttercup]'s hair",
        "  Called [Mayor]",
        "Returned to [Mojo Jojo's Observatory] to revel"
    ],
    'card_color': 'red'
}

him_actions_card = Card(**him_actions)
him_actions_card.save('cards/actions_card_him.png')

# Actions example (back of Buttercup's card) ====================
buttercup_actions = {
    'card_type': 'Actions',
    'card_title': "(back of Buttercup's card)",
    'card_body': [
        "Thwarted the [Rowdyruff Boys]",
        "Arrested by [Mayor] for [Mojo Jojo]'s murder",
        "Broke out of [Jail] to clear name",
        "Discovered [HIM] at [Mojo Jojo's Observatory]",
    ],
    'card_color': 'red'
}

buttercup_actions_card = Card(**buttercup_actions)
buttercup_actions_card.save('cards/actions_card_buttercup.png')
