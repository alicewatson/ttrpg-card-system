# Card World

A lightweight, system-agnostic, index-card-based system for managing living worlds in tabletop RPGs.

## Intro

I've played a lot of RPGs. I've run a lot of RPGs. I've spent way too many hours making detailed worlds, full of rich locations, vibrant NPCs, interesting lore, and complex plot-lines.

Then those worlds met the players and the players promptly decided to seduce a dairy cow, open a cheesecake factory, and destroy the local economy.

If I had a copper for every time that happened...

Eventually I realized that even the best-laid GM plans rarely survive first contact with the players, so I did some reading, some testing, some revising, and came up with the following system. It borrows heavily from:

- The collaborative history RPG, [Microscope](https://www.lamemage.com/microscope/).
- The lightweight [FATE](https://evilhat.com/product/fate-core-system/) / [FATE Accelerated](https://evilhat.com/product/fate-accelerated-edition/) RPG.
- [The Lazy Dungeon Master](https://slyflourish.com/the_lazy_dungeon_master_cc.html) style of GMing.
- [Five Room Dungeons](https://www.roleplayingtips.com/5-room-dungeons/), and the [5 Act Play Structure](https://www.storyboardthat.com/articles/e/five-act-structure).
- Systems I've designed for my company around AI characters and their interactions.
- And [Resource Description Framework (RDF)](https://www.w3.org/TR/2004/REC-rdf-primer-20040210/) from web development.

Without further ado, let's get started.

## The Materials

- 3x5 Index cards (preferably colored and plain)
- Pencils / pens
- A few rubber bands or an index-card organizer
- You might also want some scissors

That's pretty much it.

## The Concept

There are a few card templates, and they share a lot in common. They will all have, at the least, a name and description.

I use blue cards for locations, purple cards for plots, red / yellow / green for characters (things that have motivations and can act on them) in order from most to least impactful (e.g. the BBEG and players would be red, a travelling merchant is probably green), and I use white for pretty much everything else.

## Card Types & Relationships

- Campaigns have Settings
- Settings have Locations
- Factions have Characters
- Characters and Objects are at Locations
- Plots involve Characters, Locations, and (often) Objects
- Characters perform Actions which impact the world

!["Graph Model"](cards/model.png)

### Campaign Cards

You'll only have one of these per campaign, it serves as the dust-cover for your card deck. If you have multiple groups or systems you game with, it can be handy to note those on the card.

![campaign card](/cards/campaign_card.png)

### Setting Cards

The setting covers the broad theme and style of your campaign. Your campaign can have multiple settings, but they should be distinct. Settings contain locations, and define the things that are and aren't so in your world.

![setting card](/cards/setting_card_townsville.png)

### Location Cards

As most things have a location, location cards form the backbone of the system. Location cards have a name, a short description, an optional parent location, and an optional list of sub-locations.

![location card](cards/location_card_ppg_house.png)

![location card](/cards/location_card_mojojojo_observatory.png)

### Faction Cards

Faction cards are a way to group individual characters and to represent groups with common motivations or goals. The party is a faction, and factions can contain factions (think set theory). A faction should have 1 or more motivations.

![faction card](/cards/faction_card_powerpuff_girls.png)

![faction card](/cards/faction_card_villains.png)

### Character Cards

These aren't your character sheets, they're like cliffnotes for the GM for player characters and NPCs. You can put whatever you want on these, but I suggest you add basic facts like name, race, profession, gender, along with aspects and motivations to start. Then add any other defining info that you want to remember for the next time your players encounter the character, like speech characteristics, personality, etc that was improvised during play. I like to color code them (red, yellow, green) based on how impactful or mobile that character is, so when I flip through the cards, I can quickly pick out the characters that need the most consideration.

![character card](/cards/character_card_buttercup.png)

![character card](/cards/character_card_him.png)

### Object Cards

I only make these for major items, like plot items, named items, mcguffins, etc. It's a good idea to know who's carrying the doomsday device or where they left it.

![object card](/cards/object_card_fuzzy_shotgun.png)

### Plot Cards

Plot cards give you the key points to kick off or recall a plotline. They should have at least: a snappy title, a short description that names any locations, factions/characters, or objects that are central to the plot, and clues/hints to point the players in the right direction at each leg of the journey.

![plot card](/cards/plot_card_nomo_mojo.png)

### Actions Cards

Action cards are kind of the odd ones out in this system. They usually don't get their own card; I instead use the backs of the other cards in portrait orientation.

Generally, action cards are a list of events relating 2 or more cards. This is your log of things that happened to this faction, character, location, object, or plot.

The last action item should reflect the last major state of the card(s) it mentions.

![HIM's actions card](/cards/actions_card_him.png)

![Buttercup's actions card](/cards/actions_card_buttercup.png)

## How it works

### Layout

1. Lay out the locations as columns.
2. Lay out characters and objects as rows under their location column.
3. Slide any active plots partially under the card most recently involved with it.
4. Lay inactive cards out in a separate column.

![Layout](/canvas_nomo_mojojojo.png)

*The above image was made using an Obsidian.md canvas*<br>

I highly recommend [Obsidian.md](https://obsidian.md/) in general, but the canvas feature is also handy for this system.

### Steps

1. Look for colored cards and see what they've been up to by reading the most recent actions.
2. Determine if any of them take a new action based on their last actions, motives, and proximity to other cards.
3. Move any cards to new locations.
4. Log any notable actions.
5. Repeat.

- If any card collides with a player character, resolve it through role-play.
- Whenever there's a lull in the role-play or the players make a significant impact on the world around them, repeat the above steps.

You'll end up spending most of the time roleplaying and improvising, while occasionally shuffling a couple cards around, creating a new one, or making a note on an existing one. The system should make it a lot easier to keep a living world, alive.

## Tips & Tricks

### Plots

| Act | 3 Act Play | 5 Act Play | 5 Room Dungeon |
| --- | --- | --- | --- |
| 1 | Prologue | Prologue | Entrance |
| 2 | Rising Action | Conflict | Puzzles & Lore |
| 3 | Rising Action | Climax | Setback / Twist |
| 4 | Falling Action | Release | Boss Arc |
| 5 | Falling Action | Resolution | Reward / Reveal |

**Remember to PMS!**

Engage your players with a combination of physical, mental, and social challenges, and keep mixing it up.

### Engagement

Once per character per session (or more) aim to do one or more of the following:

1. **Raise up** - highlight what makes your players' character a badass!
2. **Slap down** - show them they still have room to grow, and make it sting.
3. **Redeem** - give a slapped character an avenue to grow or redeem their fall.

Storytelling and fun mean more than dice rolls and rules, so fudge it if it makes the session more memorable.

## Rules of Three

- 3 plot cards - keep a mix of main and side plot cards on hand
- 3 clues or hints pointing to each act of the plot or each step of the puzzle
- 3 Big Bads - the biggest should be a surprise
- 3 big locations - "big" being relative to how mobile your player characters are
- 3 zones in an encounter - give 'em some room for strategy
- 3 factions in play - even if they don't have named characters, keep them visible
- 3 personal ties to the game for each character - motives, loved ones, revenge, etc
- 3 challenge types - PMS again! Try out physical parley, mental martial arts, or a social slug-fest

## Making Your Own Cards

I included a Python script that you can use to make your own digital index cards. Feel free to mess around with it.

I also used [Obsidian.md](https://obsidian.md/)'s Canvas feature to lay the cards out for the example. It's pretty nifty.
